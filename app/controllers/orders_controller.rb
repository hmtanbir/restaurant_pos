class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_authorize, only: [:index, :destroy]
  after_action :verify_authorized, only: [:index, :destroy]

  # GET /orders
  def index
    @orders = Order.all
    respond_to do |format|
      format.html { render :index }
      format.pdf do
        pdf = OrdersPdf.new(@orders)
        send_data pdf.render, filename: 'orders.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  # GET /orders/1
  def show
    respond_to do |format|
      format.html { render :show }
      format.pdf do
        @store = Store.first
        pdf = InvoicePdf.new(@order, @store)
        send_data pdf.render, filename: "invoice-#{ @order.id }.pdf", type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  def create
    @order = Order.new(order_params)

    if @order.save
      redirect_to root_path, notice: 'Order was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      redirect_to root_path, notice: 'Order was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy if @order.present?
    redirect_to root_path, notice: 'Order was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    Rails.logger.error{ 'Order record is not found' }
    redirect_to root_url, notice: 'Data is not found !'
  end

  # Only allow a trusted parameter "white list" through.
  def order_params
    params.require(:order).permit(:discount, :brute, :net, :payed, :payed_at, :customer_id, :waiter_id, product_ids: [], table_ids: [])
  end

  # set authorizarion in model
  def set_authorize
    authorize Order
  end
end
