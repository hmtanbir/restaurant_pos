require 'rails_helper'

RSpec.describe OrdersController, type: :controller do

  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  # This should return the minimal set of attributes required to create a valid
  # Order. As you add validations to Order, be sure to
  # adjust the attributes here as well.
  let(:customer) { FactoryGirl.create :customer }
  let(:waiter) { FactoryGirl.create :waiter }
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:order, customer_id: customer.id, waiter_id: waiter.id)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:order, customer_id: nil, waiter_id: nil)
  }
 
  describe "GET #index" do
    it "returns a success response" do
      order = Order.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      order = Order.create! valid_attributes
      get :show, params: {id: order.to_param}
      expect(response).to be_success
    end

    it 'redirects to root url and show a notice if invalid is given' do
      get :show, params: { id: 100 }
      expect(response).to redirect_to(root_url)
      expect(flash[:notice]).to eq('Data is not found !')
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      order = Order.create! valid_attributes
      get :edit, params: {id: order.to_param}
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Order" do
        expect {
          post :create, params: {order: valid_attributes}
        }.to change(Order, :count).by(1)
      end

      it "redirects to the created order" do
        post :create, params: {order: valid_attributes}
        expect(response).to redirect_to(root_path)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {order: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:order, discount: '3', customer_id: customer.id, waiter_id: waiter.id)
      }

      it "updates the requested order" do
        order = Order.create! valid_attributes
        put :update, params: {id: order.to_param, order: new_attributes}
        order.reload
        expect(Order.first.discount).to eq(3)
      end

      it "redirects to the order" do
        order = Order.create! valid_attributes
        put :update, params: {id: order.to_param, order: valid_attributes}
        expect(response).to redirect_to(root_path)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        order = Order.create! valid_attributes
        put :update, params: {id: order.to_param, order: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested order" do
      order = Order.create! valid_attributes
      expect {
        delete :destroy, params: {id: order.to_param}
      }.to change(Order, :count).by(-1)
    end

    it "redirects to the orders list" do
      order = Order.create! valid_attributes
      delete :destroy, params: {id: order.to_param}
      expect(response).to redirect_to(root_path)
    end
  end

end
