require 'rails_helper'

RSpec.describe WaitersController, type: :controller do

  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end

  # This should return the minimal set of attributes required to create a valid
  # Waiter. As you add validations to Waiter, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:waiter)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:waiter, name: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      waiter = Waiter.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      waiter = Waiter.create! valid_attributes
      get :show, params: {id: waiter.to_param}
      expect(response).to be_success
    end

    it 'redirects to root url and show a notice if invalid is given' do
      get :show, params: { id: 100 }
      expect(response).to redirect_to(root_url)
      expect(flash[:notice]).to eq('Data is not found !')
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      waiter = Waiter.create! valid_attributes
      get :edit, params: {id: waiter.to_param}
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Waiter" do
        expect {
          post :create, params: {waiter: valid_attributes}
        }.to change(Waiter, :count).by(1)
      end

      it "redirects to the created waiter" do
        post :create, params: {waiter: valid_attributes}
        expect(response).to redirect_to(Waiter.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {waiter: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:waiter, name: 'Adam')
      }

      it "updates the requested waiter" do
        waiter = Waiter.create! valid_attributes
        put :update, params: {id: waiter.to_param, waiter: new_attributes}
        waiter.reload
        expect(Waiter.first.name).to eq('Adam')
      end

      it "redirects to the waiter" do
        waiter = Waiter.create! valid_attributes
        put :update, params: {id: waiter.to_param, waiter: valid_attributes}
        expect(response).to redirect_to(waiter)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        waiter = Waiter.create! valid_attributes
        put :update, params: {id: waiter.to_param, waiter: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested waiter" do
      waiter = Waiter.create! valid_attributes
      expect {
        delete :destroy, params: {id: waiter.to_param}
      }.to change(Waiter, :count).by(-1)
    end

    it "redirects to the waiters list" do
      waiter = Waiter.create! valid_attributes
      delete :destroy, params: {id: waiter.to_param}
      expect(response).to redirect_to(waiters_url)
    end
  end

end
