FactoryGirl.define do
  factory :user do
    email 'admin@example.com'
    password 'secret'
    name 'Admin'
    role 'admin'
  end
end
