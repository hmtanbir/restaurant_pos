FactoryGirl.define do
  factory :order do
    discount "9.99"
    brute "3.33"
    net "9.99"
    payed false
    payed_at "2017-10-03 19:56:50"
    customer nil
    waiter nil
  end
end
