class UserPolicy < ApplicationPolicy
  def rails_admin?(action)
    case action
      when :destroy, :edit, :new
        if user.admin?
          true
        else
          false
        end
      else
        super
    end
  end
end
