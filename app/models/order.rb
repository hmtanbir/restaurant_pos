class Order < ApplicationRecord
  include RailsAdminCharts
  include OrdersHelper
  include SharedScopes

  belongs_to :customer
  belongs_to :waiter
  validates_presence_of :brute, :net
  has_and_belongs_to_many :products, dependent: :destroy
  has_and_belongs_to_many :tables, dependent: :destroy
  scope :pending, -> { where(payed: false) }
  scope :payed, -> { where(payed: true).order(payed_at: 'desc') }
end
