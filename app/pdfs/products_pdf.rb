class ProductsPdf
## `include` instead of subclassing Prawn::Document
## as advised by the official manual

  include Prawn::View

  def initialize(products)
    @products = products
    content
  end

  def content
    product_table
    footer
  end

  def product_table
    text 'Products Report', style: :bold_italic
    stroke_horizontal_rule

    temp_arr = []
    @products.each do |product|
      item = {
        name: product.try(:name),
        price: product.try(:price).to_s,
        description: product.try(:description),
        category: product.category.try(:name)
      }
      temp_arr << item
    end

    move_down 10
    items = [['No.','Name', 'Price', 'Description', 'Category' ]]
    items += temp_arr.each_with_index.map do |item, i|
      [
        i+1,
        item[:name],
        '$'+item[:price],
        item[:description],
        item[:category],
      ]
    end

    table items, header: true,
          column_widths: { 0 => 50, 1 => 70, 2=> 100 , 3 => 220, 4=> 100}, row_colors: ['f7f7f7', 'FFFFFF'] do
      style(columns(5)) {|x| x.align = :right }
    end
  end

  def footer
    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end
end
