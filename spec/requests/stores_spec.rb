require 'rails_helper'

RSpec.describe 'Stores', type: :request do
  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  describe 'GET /stores' do
    it 'works!' do
      get stores_path
      expect(response).to have_http_status(200)
    end
  end
end
