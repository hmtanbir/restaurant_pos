FactoryGirl.define do
  factory :customer do
    name 'John Doe'
    address 'New Delhi, India'
    email 'j@d.com'
    birthday '2017-10-03'
    phone '12334'
  end
end
