require 'rails_helper'

RSpec.describe 'Products', type: :request do
  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  describe 'GET /products' do
    it 'works!' do
      get products_path
      expect(response).to have_http_status(200)
    end
  end
end
