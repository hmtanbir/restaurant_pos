class StoresController < ApplicationController
  before_action :set_store, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_authorize, only: [:index, :destroy]
  after_action :verify_authorized, only: [:index, :destroy]

  # GET /stores
  def index
    @stores = Store.all
    respond_to do |format|
      format.html { render :index }
      format.pdf do
        pdf = StoresPdf.new(@stores)
        send_data pdf.render, filename: 'stores.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  # GET /stores/1
  def show
  end

  # GET /stores/new
  def new
    @store = Store.new
  end

  # GET /stores/1/edit
  def edit
  end

  # POST /stores
  def create
    @store = Store.new(store_params)

    if @store.save
      redirect_to @store, notice: 'Store was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /stores/1
  def update
    if @store.update(store_params)
      redirect_to @store, notice: 'Store was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /stores/1
  def destroy
    @store.destroy if @store.present?
    redirect_to stores_url, notice: 'Store was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_store
    @store = Store.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    Rails.logger.error{ 'Store record is not found' }
    redirect_to root_url, notice: 'Data is not found !'
  end

  # Only allow a trusted parameter "white list" through.
  def store_params
    params.require(:store).permit(:name, :description, :phone, :address, :tax_rate, :website)
  end

  # set authorizarion in model
  def set_authorize
    authorize Store
  end

end
