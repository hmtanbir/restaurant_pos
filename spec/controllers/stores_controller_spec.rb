require 'rails_helper'

RSpec.describe StoresController, type: :controller do

  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  # This should return the minimal set of attributes required to create a valid
  # Store. As you add validations to Store, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:store)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:store, name: nil)
  }


  describe "GET #index" do
    it "returns a success response" do
      store = Store.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      store = Store.create! valid_attributes
      get :show, params: { id: store.to_param }
      expect(response).to be_success
    end

    it 'redirects to root url and show a notice if invalid is given' do
      get :show, params: { id: 100 }
      expect(response).to redirect_to(root_url)
      expect(flash[:notice]).to eq('Data is not found !')
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      store = Store.create! valid_attributes
      get :edit, params: { id: store.to_param }
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Store" do
        expect {
          post :create, params: { store: valid_attributes }
        }.to change(Store, :count).by(1)
      end

      it "redirects to the created store" do
        post :create, params: {store: valid_attributes}
        expect(response).to redirect_to(Store.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {store: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:store, name: 'store1_new')
      }

      it "updates the requested store" do
        store = Store.create! valid_attributes
        put :update, params: {id: store.to_param, store: new_attributes}
        store.reload
        expect(Store.first.name).to eq('store1_new')
      end

      it "redirects to the store" do
        store = Store.create! valid_attributes
        put :update, params: {id: store.to_param, store: valid_attributes}
        expect(response).to redirect_to(store)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        store = Store.create! valid_attributes
        put :update, params: { id: store.to_param, store: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested store" do
      store = Store.create! valid_attributes
      expect {
        delete :destroy, params: { id: store.to_param }
      }.to change(Store, :count).by(-1)
    end

    it "redirects to the stores list" do
      store = Store.create! valid_attributes
      delete :destroy, params: {id: store.to_param}
      expect(response).to redirect_to(stores_url)
    end
  end

end
