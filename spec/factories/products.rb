FactoryGirl.define do
  factory :product do
    name 'Burger'
    price '9.99'
    description 'Hot beef burger'
    category nil
  end
end
