require 'rails_helper'

RSpec.describe OrdersHelper, type: :helper do
  let(:customer) { FactoryGirl.create :customer }
  let(:waiter) { FactoryGirl.create :waiter }
  let(:category) { FactoryGirl.create :category }
  let(:product) { FactoryGirl.create :product, category_id: category.id }
  let(:table_1) { FactoryGirl.create :table, name: 'East' }
  let(:table_2) { FactoryGirl.create :table, name: 'West' }
  let(:order_with_tables) do
    FactoryGirl.create :order,
                       customer_id: customer.id,
                       waiter_id: waiter.id,
                       product_ids: [product.id],
                       table_ids: [table_1.id, table_2.id]
  end
  let(:order_without_tables) do
    FactoryGirl.create :order,
                       customer_id: customer.id,
                       waiter_id: waiter.id,
                       product_ids: [product.id],
                       table_ids: []
  end

  describe 'tables_list' do
    context 'when tables are available' do
      it 'returns order tables name list' do
        expect(tables_list(order_with_tables)).to eq("#{ table_1.name }, #{ table_2.name }")
      end
    end

    context 'when tables are not available' do
      it 'returns empty' do
        expect(tables_list(order_without_tables)).to be_empty
      end
    end
  end
end
