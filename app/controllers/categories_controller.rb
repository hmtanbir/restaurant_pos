class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_authorize, only: [:index, :destroy]
  after_action :verify_authorized, only: [:index, :destroy]

  # GET /categories
  def index
    @categories = Category.all
    respond_to do |format|
      format.html { render :index }
      format.pdf do
        pdf = CategoriesPdf.new(@categories)
        send_data pdf.render, filename: 'categories.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  # GET /categories/1
  def show
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  def create
    @category = Category.new(category_params)

    if @category.save
      redirect_to @category, notice: 'Category was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /categories/1
  def update
    if @category.update(category_params)
      redirect_to @category, notice: 'Category was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /categories/1
  def destroy
    @category.destroy if @category.present?
    redirect_to categories_url, notice: 'Category was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = Category.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    Rails.logger.error{ 'Category record is not found' }
    redirect_to root_url, notice: 'Data is not found !'
  end

  # Only allow a trusted parameter "white list" through.
  def category_params
    params.require(:category).permit(:name, :description)
  end

  # set authorizarion in model
  def set_authorize
    authorize Category
  end
end
