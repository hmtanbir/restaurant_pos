class StoresPdf
## `include` instead of subclassing Prawn::Document
## as advised by the official manual

  include Prawn::View

  def initialize(stores)
    @stores = stores
    content
  end

  def content
    store_table
    footer
  end

  def store_table
    text 'Stores Report', style: :bold_italic
    stroke_horizontal_rule

    temp_arr = []
    @stores.each do |store|
      item = {
        name: store.try(:name),
        description: store.try(:description),
        phone: store.try(:phone),
        address: store.try(:address),
        tax_rate: store.try(:tax_rate).to_s,
        website: store.try(:website)
      }

      temp_arr << item
    end

    move_down 10
    items = [['No.','Name', 'Description', 'Phone', 'Address', 'Tax rate', 'Website']]
    items += temp_arr.each_with_index.map do |item, i|
      [
        i+1,
        item[:name],
        item[:description],
        item[:phone],
        item[:address],
        item[:tax_rate]+'%',
        item[:website]
      ]
    end

    table items, header: true,
          column_widths: { 0 => 50, 1 => 75, 2 => 105, 3=>75, 4=> 85, 5=> 75, 6=> 75}, row_colors: ['f7f7f7', 'FFFFFF'] do
      style(columns(7)) {|x| x.align = :right }
    end
  end

  def footer
    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end
end
