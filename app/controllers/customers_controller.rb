class CustomersController < ApplicationController
  before_action :set_customer, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_authorize, only: [:index, :destroy]
  after_action :verify_authorized, only: [:index, :destroy]

  # GET /customers
  def index
    @customers = Customer.all
    respond_to do |format|
      format.html { render :index }
      format.pdf do
        pdf = CustomersPdf.new(@customers)
        send_data pdf.render, filename: 'customers.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  # GET /customers/1
  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  def create
    @customer = Customer.new(customer_params)

    if @customer.save
      redirect_to @customer, notice: 'Customer was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /customers/1
  def update
    if @customer.update(customer_params)
      redirect_to @customer, notice: 'Customer was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /customers/1
  def destroy
    @customer.destroy if @customer.present?
    redirect_to customers_url, notice: 'Customer was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_customer
    @customer = Customer.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    Rails.logger.error{ 'Customer record is not found' }
    redirect_to root_url, notice: 'Data is not found !'
  end

  # Only allow a trusted parameter "white list" through.
  def customer_params
    params.require(:customer).permit(:name, :address, :email, :birthday, :phone)
  end

  # set authorizarion in model
  def set_authorize
    authorize Customer
  end
end
