class CustomersPdf
## `include` instead of subclassing Prawn::Document
## as advised by the official manual

  include Prawn::View

  def initialize(customers)
    @customers = customers
    content
  end

  def content
    customer_table
    footer
  end

  def customer_table
    text 'Customers Report', style: :bold_italic
    stroke_horizontal_rule

    temp_arr = []
    @customers.each do |customer|
      item = {
        name: customer.try(:name),
        email: customer.try(:email),
        birthday: customer.try(:birthday).to_s,
        phone: customer.try(:phone)
        }
      temp_arr << item
    end

    move_down 10
    items = [['No','Name', 'Email', 'Birtday', 'phone' ]]
    items += temp_arr.each_with_index.map do |item, i|
      [
        i+1,
        item[:name],
        item[:email],
        item[:birthday],
        item[:phone],
      ]
    end

    table items, header: true,
          column_widths: { 0 => 50, 1 => 120, 2=> 150, 3=> 100, 4 => 120}, row_colors: ['f7f7f7', 'FFFFFF'] do
      style(columns(5)) {|x| x.align = :right }
    end
  end

  def footer
    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end
end
