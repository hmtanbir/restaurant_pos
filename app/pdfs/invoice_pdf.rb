class InvoicePdf
  ## `include` instead of subclassing Prawn::Document
  ## as advised by the official manual

  include Prawn::View

  def initialize(order, store)
    @order = order
    @store = store
    content
  end

  def content
    define_grid(columns: 5, rows:  8, gutter: 10)
    customer_data
    company_data
    invoice_table
    footer
  end

  def customer_data
    grid([0,0], [1,1]).bounding_box do
      text  "INVOICE", size: 18
      text "Invoice No: #{@order.id}", align: :left
      text "Date: #{Date.today.to_s}", align: :left
      move_down 10

      text "Customer Name: #{ @order.customer.name.titleize }"
      text "Customer E-mail: #{ @order.customer.email }"
      text "Customer Phone: #{ @order.customer.phone }"
    end
  end

  def company_data
    grid([0,3.6], [1,4]).bounding_box do
      # Company address
      move_down 10
      text "#{ @store.name.titleize }", align: :left
      text "#{ @store.address.titleize }", align: :left
      text "#{ @store.phone }", align: :left
      move_down 20
      text "Served by: #{ @order.waiter.name.titleize }", align: :left
    end
  end
  
  def invoice_table
    text 'Details of Invoice', style: :bold_italic
    stroke_horizontal_rule

    temp_arr = []
    @order.products.each do |product|
      item = { name: product.name.titleize, price: product.price.to_s }
      temp_arr << item
    end

    move_down 10
    items = [['No','Description', 'Qt.', 'Price']]
    items += temp_arr.each_with_index.map do |item, i|
      [
        i + 1,
        item[:name],
        '1',
        '$'+item[:price]
      ]
    end
    items += [['', 'Sub-total', '', "$#{ @order.try(:brute) }"]]
    items += [['', 'Discount', '', "#{ @order.try(:discount) }%"]]
    items += [['', 'Tax', '', "#{ @store.try(:tax_rate) }%"]]
    items += [['', 'Total', '', "$#{@order.try(:net)}"]]


    table items, header: true,
              column_widths: { 0 => 50, 1 => 350, 3 => 100}, row_colors: ['c5c5c5', 'FFFFFF'] do
      style(columns(3)) {|x| x.align = :right }
    end
  end

  def footer
    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end
end
