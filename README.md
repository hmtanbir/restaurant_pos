# Restaurant Point of Sale Software

This project is powered by Ruby on rails, bootstrap 3.

# Installation Process

**Unzip the folder** and open terminal and write: 

**$ cd restaurant_pos**

Install bundler : **$ gem install bundler --pre**

Install college : **$ bundle install**

# Database Setup

Open `config-> database.yml` and **change in your Mysql database username and password**

**Database creation :** $ `bundle exec rake db:create`

**Database migrate :** $ `bundle exec rake db:migrate`

**Database seed :** $ `bundle exec rake db:seed`

# Run server
open terminal and run
$ `rails s`

# How it Works ?

Restaurant POS Software is a simple POS software for a small restaurant.
## Features

**User feature**

It has amazing user features which has two types. User has to be sign in before
entering the home page or admin page. It contains **two types** authorization.

1. Admin
2. Staff

**Admin** can create, edit and delete all other features and can access
the reports and download all reports.

**Staff** can create and edit some limited features but can not access
reports. 

**Category feature**

Category feature contains name and description of a product types.
It tells us product is food or drink type. It contains `name` and `description` attributes.
Category has many products.

**Customer feature**

Customer feature contains data of restaurant customers. It contains `name`, `address`, `birthday`, `email` and
`phone` attributes. Customer has many orders.

**Product feature**

Product feature contains data of restaurant product that means foods, drinks etc.
It contains `name`, `price`, `description`, `category` attributes. Product belongs to category.

**Waiter feature**

Waiter feature contains data of restaurant waiters. It contains `name`, `address` and `phone` attributes.
Waiter has many orders.

**Table feature**

Table feature contains data of restaurant tables. It contains `name`, `capacity` and `indoor` attributes.
Table has many orders.

**Order feature**

Order feature contains data of restaurant orders. It contains `discount`, `brute`, `net`, `payed` , `payed at`
attributes. Order has many products and tables.

# Tools used
Ruby (version 2.4.1)

Ruby on Rails(version 5.1)

Mysql

Bootstrap 3 framework

rails-admin themes

Jquery/CoffeeScript

Prawn

Jquery Data table

RSpec, FactoryGirl

# Testing

Run all testing: `bundle exec rspec spec`

Testing coverage above 90%

# Feedback
Since ruby version 2.2.3 have lacking of security and does not
support in future, so we should use latest ruby version for making software.
Many gem as like as devise, devise-bootstrapped, rails-admin are not support ruby 2.2.3 version.
Therefore, In this application, we use ruby version 2.4.1 for application
stability.
