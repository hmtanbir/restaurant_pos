class Product < ApplicationRecord
  include RailsAdminCharts
  belongs_to :category
  validates_presence_of :name, :price
  validates_uniqueness_of :name

  def name_with_price
    "#{self.name} - $#{self.price}"
  end
end
