class TablesPdf
## `include` instead of subclassing Prawn::Document
## as advised by the official manual

  include Prawn::View

  def initialize(tables)
    @tables = tables
    content
  end

  def content
    restaurant_tables_table
    footer
  end

  def restaurant_tables_table
    text 'Tables Report', style: :bold_italic
    stroke_horizontal_rule

    temp_arr = []
    @tables.each do |table|
      item = {
        name: table.try(:name),
        capacity: table.try(:capacity),
        indoor: table.try(:indoor).to_s
      }

      temp_arr << item
    end

    move_down 10
    items = [['No.','Name', 'Capacity', 'Indoor']]
    items += temp_arr.each_with_index.map do |item, i|
      [
        i+1,
        item[:name],
        item[:capacity],
        item[:indoor]
      ]
    end

    table items, header: true,
          column_widths: { 0 => 50, 1 => 200, 2 => 150, 3=>140 }, row_colors: ['f7f7f7', 'FFFFFF'] do
      style(columns(4)) {|x| x.align = :right }
    end
  end

  def footer
    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end
end
