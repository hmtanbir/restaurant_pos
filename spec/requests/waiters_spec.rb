require 'rails_helper'

RSpec.describe 'Waiters', type: :request do
  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  describe 'GET /waiters' do
    it 'works!' do
      get waiters_path
      expect(response).to have_http_status(200)
    end
  end
end
