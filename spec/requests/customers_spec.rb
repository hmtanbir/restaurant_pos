require 'rails_helper'

RSpec.describe "Customers", type: :request do
  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  describe "GET /customers" do
    it "works!" do
      get customers_path
      expect(response).to have_http_status(200)
    end
  end
end
