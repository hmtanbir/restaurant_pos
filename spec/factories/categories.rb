FactoryGirl.define do
  factory :category do
    name 'food'
    description 'food description'
  end
end
