Rails.application.routes.draw do
  root to: 'home#index'

  resources :orders
  resources :products
  resources :customers
  resources :tables
  resources :waiters
  resources :stores
  resources :categories
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
