require 'rails_helper'

RSpec.describe CustomersController, type: :controller do

  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  # This should return the minimal set of attributes required to create a valid
  # Customer. As you add validations to Customer, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:customer)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:customer, name: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      customer = Customer.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      customer = Customer.create! valid_attributes
      get :show, params: {id: customer.to_param}
      expect(response).to be_success
    end

    it 'redirects to root url and show a notice if invalid is given' do
      get :show, params: { id: 100 }
      expect(response).to redirect_to(root_url)
      expect(flash[:notice]).to eq('Data is not found !')
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      customer = Customer.create! valid_attributes
      get :edit, params: {id: customer.to_param}
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Customer" do
        expect {
          post :create, params: {customer: valid_attributes}
        }.to change(Customer, :count).by(1)
      end

      it "redirects to the created customer" do
        post :create, params: {customer: valid_attributes}
        expect(response).to redirect_to(Customer.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {customer: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:customer, name: 'David')
      }

      it "updates the requested customer" do
        customer = Customer.create! valid_attributes
        put :update, params: {id: customer.to_param, customer: new_attributes}
        customer.reload
        expect(Customer.first.name).to eq('David')
      end

      it "redirects to the customer" do
        customer = Customer.create! valid_attributes
        put :update, params: {id: customer.to_param, customer: valid_attributes}
        expect(response).to redirect_to(customer)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        customer = Customer.create! valid_attributes
        put :update, params: {id: customer.to_param, customer: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested customer" do
      customer = Customer.create! valid_attributes
      expect {
        delete :destroy, params: {id: customer.to_param}
      }.to change(Customer, :count).by(-1)
    end

    it "redirects to the customers list" do
      customer = Customer.create! valid_attributes
      delete :destroy, params: {id: customer.to_param}
      expect(response).to redirect_to(customers_url)
    end
  end

end
