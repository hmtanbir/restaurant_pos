require 'rails_helper'

RSpec.describe Order, type: :model do
  it { is_expected.to belong_to :customer }
  it { is_expected.to belong_to :waiter }
  it { is_expected.to validate_presence_of :brute }
  it { is_expected.to validate_presence_of :net }
  it { is_expected.to have_and_belong_to_many :tables }
  it { is_expected.to have_and_belong_to_many :products }

  let(:customer) { FactoryGirl.create :customer }
  let(:waiter) { FactoryGirl.create :waiter }
  let!(:order_with_paid) { FactoryGirl.create :order, payed: true, customer_id: customer.id, waiter_id: waiter.id }
  let!(:order_without_paid) { FactoryGirl.create :order, payed: false, customer_id: customer.id, waiter_id: waiter.id }

  describe '.pending' do
    it 'returns all pending orders' do
      expect(described_class.pending).to eq([order_without_paid])
    end
  end

  describe '.payed' do
    it 'returns payed orders' do
      expect(described_class.payed).to eq([order_with_paid])
    end
  end
end
