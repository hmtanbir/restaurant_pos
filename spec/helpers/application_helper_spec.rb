require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:store) { FactoryGirl.create :store }

  describe 'app_name' do
    context 'when store is available' do
      before do
        store
      end

      it 'returns name of store' do
        expect(app_name).to eq(store.name)
      end
    end

    context 'when store is not available' do
      it 'returns Restaurant POS' do
        expect(app_name).to eq('Restaurant POS')
      end
    end
  end
end
