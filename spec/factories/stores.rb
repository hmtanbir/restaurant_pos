FactoryGirl.define do
  factory :store do
    name 'Store 1'
    description 'Store 1 description'
    phone '1234'
    address 'Store address'
    tax_rate '9.99'
    website 'store.website'
  end
end
