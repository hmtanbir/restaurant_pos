FactoryGirl.define do
  factory :waiter do
    name 'John Doe'
    address 'New York'
    phone '1233'
  end
end
