require 'rails_helper'

RSpec.describe 'Orders', type: :request do
  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  describe 'GET /orders' do
    it 'works!' do
      get orders_path
      expect(response).to have_http_status(200)
    end
  end
end
