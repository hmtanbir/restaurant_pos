class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def rails_admin?(action)
    case action
      when :dashboard
        user.admin? || user.staff?
      when :index
        user.admin? || user.staff?
      when :show
        user.admin? || user.staff?
      when :new
        user.admin? || user.staff?
      when :edit
        user.admin? || user.staff?
      when :destroy
        user.admin?
      when :export
        user.admin?
      when :history
        user.admin?
      when :show_in_app
        user.admin?
      when :charts
        user.admin?
      else
        raise ::Pundit::NotDefinedError, "unable to find policy #{action} for #{record}."
    end
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
