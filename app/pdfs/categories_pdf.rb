class CategoriesPdf
## `include` instead of subclassing Prawn::Document
## as advised by the official manual

    include Prawn::View

    def initialize(categories)
      @categories = categories
      content
    end

    def content
      category_table
      footer
    end

    def category_table
      text 'Categories Report', style: :bold_italic
      stroke_horizontal_rule

      temp_arr = []
      @categories.each do |category|
        item = {
          name: category.try(:name),
          description: category.try(:description)
        }

        temp_arr << item
      end

      move_down 10
      items = [['No','Name', 'Description']]
      items += temp_arr.each_with_index.map do |item, i|
        [
          i+1,
          item[:name],
          item[:description]

        ]
      end

      table items, header: true,
            column_widths: { 0 => 100, 1 => 180, 2 => 250}, row_colors: ['f7f7f7', 'FFFFFF'] do
        style(columns(3)) {|x| x.align = :right }
      end
    end

    def footer
      bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
        pagecount = page_count
        text "Page #{pagecount}"
      end
    end
  end
