class WaitersPdf
## `include` instead of subclassing Prawn::Document
## as advised by the official manual

  include Prawn::View

  def initialize(waiters)
    @waiters = waiters
    content
  end

  def content
    restarant_waiters_table
    footer
  end

  def restarant_waiters_table
    text 'Waiters Report', style: :bold_italic
    stroke_horizontal_rule

    temp_arr = []
    @waiters.each do |waiter|
      item = {
        name: waiter.try(:name),
        address: waiter.try(:address),
        phone: waiter.try(:phone)
      }

      temp_arr << item
    end

    move_down 10
    items = [['No.','Name', 'Address', 'Phone']]
    items += temp_arr.each_with_index.map do |item, i|
      [
        i+1,
        item[:name],
        item[:address],
        item[:phone]
      ]
    end

    table items, header: true,
          column_widths: { 0 => 50, 1 => 150, 2 => 200, 3=>140 }, row_colors: ['f7f7f7', 'FFFFFF'] do
      style(columns(4)) {|x| x.align = :right }
    end
  end

  def footer
    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end
end
