class User < ApplicationRecord
  include RailsAdminCharts
  enum role: [:staff, :admin]
  after_initialize :set_default_role, if: :new_record?

  def set_default_role
    self.role ||= :staff
  end

  validates_presence_of :name
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
