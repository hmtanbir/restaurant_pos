class OrdersPdf
## `include` instead of subclassing Prawn::Document
## as advised by the official manual

  include Prawn::View

  def initialize(orders)
    @orders = orders
    content
  end

  def content
    order_table
    footer
  end

  def order_table
    text 'Orders Report', style: :bold_italic
    stroke_horizontal_rule

    temp_arr = []
    @orders.each do |order|
      item = {
        brute: order.try(:brute).to_s,
        discount: order.try(:discount).to_s,
        net: order.try(:net).to_s,
        payed: order.try(:payed).to_s,
        paid_at: order.try(:payed_at).to_s,
        customer_name: order.customer.name.titleize,
        customer_phone: order.customer.phone,
        waiter_name: order.waiter.name.titleize
      }

      temp_arr << item
    end

    move_down 10
    items = [['No','Sub-total', 'Discount', 'Net', 'Paid', 'Paid at', 'Customer Name', 'Customer phone', 'Waiter' ]]
    items += temp_arr.each_with_index.map do |item, i|
      [
        i+1,
        '$'+item[:brute],
        item[:discount]+'%',
        '$'+item[:net],
        item[:payed],
        item[:paid_at],
        item[:customer_name],
        item[:customer_phone],
        item[:waiter_name],
      ]
    end

    table items, header: true,
          column_widths: { 0 => 30, 1 => 60, 3 => 50, 4=>40, 5=>80, 6=> 70, 7=>80, 8=>70}, row_colors: ['f7f7f7', 'FFFFFF'] do
      style(columns(9)) {|x| x.align = :right }
    end
  end

  def footer
    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end
end
