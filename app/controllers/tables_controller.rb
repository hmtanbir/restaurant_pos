class TablesController < ApplicationController
  before_action :set_table, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_authorize, only: [:index, :destroy]
  after_action :verify_authorized, only: [:index, :destroy]

  # GET /tables
  def index
    @tables = Table.all
    respond_to do |format|
      format.html { render :index }
      format.pdf do
        pdf = TablesPdf.new(@tables)
        send_data pdf.render, filename: 'tables.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  # GET /tables/1
  def show
  end

  # GET /tables/new
  def new
    @table = Table.new
  end

  # GET /tables/1/edit
  def edit
  end

  # POST /tables
  def create
    @table = Table.new(table_params)

    if @table.save
      redirect_to @table, notice: 'Table was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /tables/1
  def update
    if @table.update(table_params)
      redirect_to @table, notice: 'Table was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /tables/1
  def destroy
    @table.destroy if @table.present?
    redirect_to tables_url, notice: 'Table was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_table
    @table = Table.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    Rails.logger.error{ 'Table record is not found' }
    redirect_to root_url, notice: 'Data is not found !'
  end

  # Only allow a trusted parameter "white list" through.
  def table_params
    params.require(:table).permit(:name, :capacity, :indoor)
  end

  # set authorizarion in model
  def set_authorize
    authorize Table
  end

end
