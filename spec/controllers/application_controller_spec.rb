require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  let(:store) { FactoryGirl.create :store }
  let(:subject) { described_class.new }

  describe '#set_taxes' do
    context 'when store is available' do
      before do
        store
      end

      it 'returns tax rate of store' do
        expect(subject.set_taxes).to eq(store.tax_rate)
      end
    end

    context 'when store is not available' do
      it 'returns nil' do
        expect(subject.set_taxes).to eq(nil)
      end
    end
  end
end
