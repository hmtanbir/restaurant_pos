class Store < ApplicationRecord
  include RailsAdminCharts
  validates_presence_of :name, :phone, :tax_rate
  validates_uniqueness_of :name, :phone, :website
end
