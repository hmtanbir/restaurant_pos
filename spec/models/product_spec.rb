require 'rails_helper'

RSpec.describe Product, type: :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :price }
  it { is_expected.to validate_uniqueness_of :name }

  describe '#name_with_price' do
    let(:category) { FactoryGirl.create :category }
    let(:product) { FactoryGirl.create :product, category_id: category.id }

    it 'returns product name with price' do
      expect(product.name_with_price).to eq("#{product.name} - $#{product.price}")
    end
  end
end
