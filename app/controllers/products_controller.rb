class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_authorize, only: [:index, :destroy]
  after_action :verify_authorized, only: [:index, :destroy]

  # GET /products
  def index
    @products = Product.all
    respond_to do |format|
      format.html { render :index }
      format.pdf do
        pdf = ProductsPdf.new(@products)
        send_data pdf.render, filename: 'products.pdf', type: 'application/pdf', disposition: 'inline'
      end
    end
  end

  # GET /products/1
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to @product, notice: 'Product was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy if @product.present?
    redirect_to products_url, notice: 'Product was successfully destroyed.'
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_product
    @product = Product.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    Rails.logger.error{ 'Product record is not found' }
    redirect_to root_url, notice: 'Data is not found !'
  end

  # Only allow a trusted parameter "white list" through.
  def product_params
    params.require(:product).permit(:name, :price, :description, :category_id)
  end

  # set authorizarion in model
  def set_authorize
    authorize Product
  end

end
