FactoryGirl.define do
  factory :table do
    name 'Table 1'
    capacity 1
    indoor false
  end
end
