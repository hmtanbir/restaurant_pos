class ApplicationController < ActionController::Base
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  protect_from_forgery prepend: true

  before_action :set_taxes

  def set_taxes
    store = Store.first
    @taxes = store.tax_rate unless store.nil?
  end

  private

  def user_not_authorized
    flash[:alert] = 'Access denied.'
    redirect_to(request.referrer || root_path)
  end
end
