require 'rails_helper'

RSpec.describe "Tables", type: :request do
  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  describe "GET /tables" do
    it "works!" do
      get tables_path
      expect(response).to have_http_status(200)
    end
  end
end
