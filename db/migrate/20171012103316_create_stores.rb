class CreateStores < ActiveRecord::Migration[5.1]
  def change
    create_table :stores do |t|
      t.string :name
      t.text :description
      t.string :phone
      t.string :address
      t.decimal :tax_rate
      t.string :website

      t.timestamps
    end
  end
end
