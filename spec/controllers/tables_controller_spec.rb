require 'rails_helper'

RSpec.describe TablesController, type: :controller do

  let(:user) { FactoryGirl.create :user }
  before do
    sign_in(user)
  end
  # This should return the minimal set of attributes required to create a valid
  # Table. As you add validations to Table, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    FactoryGirl.attributes_for(:table)
  }

  let(:invalid_attributes) {
    FactoryGirl.attributes_for(:table, name: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      table = Table.create! valid_attributes
      get :index
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      table = Table.create! valid_attributes
      get :show, params: {id: table.to_param}
      expect(response).to be_success
    end

    it 'redirects to root url and show a notice if invalid is given' do
      get :show, params: { id: 100 }
      expect(response).to redirect_to(root_url)
      expect(flash[:notice]).to eq('Data is not found !')
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      table = Table.create! valid_attributes
      get :edit, params: {id: table.to_param}
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Table" do
        expect {
          post :create, params: {table: valid_attributes}
        }.to change(Table, :count).by(1)
      end

      it "redirects to the created table" do
        post :create, params: {table: valid_attributes}
        expect(response).to redirect_to(Table.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {table: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        FactoryGirl.attributes_for(:table, name: 'table new')
      }

      it "updates the requested table" do
        table = Table.create! valid_attributes
        put :update, params: {id: table.to_param, table: new_attributes}
        table.reload
        expect(Table.first.name).to eq('table new')
      end

      it "redirects to the table" do
        table = Table.create! valid_attributes
        put :update, params: {id: table.to_param, table: valid_attributes}
        expect(response).to redirect_to(table)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        table = Table.create! valid_attributes
        put :update, params: {id: table.to_param, table: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested table" do
      table = Table.create! valid_attributes
      expect {
        delete :destroy, params: {id: table.to_param}
      }.to change(Table, :count).by(-1)
    end

    it "redirects to the tables list" do
      table = Table.create! valid_attributes
      delete :destroy, params: {id: table.to_param}
      expect(response).to redirect_to(tables_url)
    end
  end

end
