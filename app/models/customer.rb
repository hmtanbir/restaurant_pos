class Customer < ApplicationRecord
  include SharedScopes
  include RailsAdminCharts
  validates_presence_of :name, :address
  validates_uniqueness_of :email, :phone
  has_many :orders, dependent: :destroy
end
